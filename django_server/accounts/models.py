from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    first_name = models.CharField(_("first name"), max_length=200, blank=True)
    last_name = models.CharField(_("last name"), max_length=200, blank=True)

