"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count

import os


def max_workers():
    return os.getenv("GUNICORN_WORKERS", None) or (2 * cpu_count() + 1)


def max_threads():
    return os.getenv("GUNICORN_THREADS", None) or (2 * cpu_count())


max_requests = int(os.getenv("GUNICORN_MAX_REQUESTS", None) or 1000)
max_requests_jitter = int(max_requests * 0.1)
worker_class = "gthread"
workers = max_workers()
threads = max_threads()
timeout = 300

name = "django-server"
