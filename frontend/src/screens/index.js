import React, {useEffect, useState} from "react";
import {UsersAPI} from "../API/users/UsersAPI";

export const BaseScreen = () => {

  const [users, setUsers] = useState([]);

  useEffect(() => {
    const retrieveUsersList = async () => {
      try {
        const response = await UsersAPI.getList();
        setUsers(response.data)
      } catch (error) {
        console.log("Error: ", error)
      }
    };

    retrieveUsersList();
  }, []);

  return (
    <div>
      Hello React World

      {users.map(user => {
        return (
          <div>
            <p> {user.first_name}</p>
            <p> {user.last_name}</p>
          </div>

        );
      })
      }
    </div>

  );
};
