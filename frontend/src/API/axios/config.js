import Cookies from "js-cookie";

export const axiosConfig = {
  timeout: 30000,
  headers: {
    "X-CSRFToken": Cookies.get("csrftoken"),
    "Content-Type": "application/json"
  },
  withCredentials: true
};
