import axios from "axios";
import {axiosConfig} from "../axios/config";

const instance = axios.create({
  ...axiosConfig,
  baseURL: "/api/v1/users"
});

export const UsersAPI = {
  get: userId => {
    return instance.get("/" + userId + "/");
  },

  getList: () => {
    return instance.get("/");
  }
};
