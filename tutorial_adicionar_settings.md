# Como o projeto é hibrido, precisamos dar um build no frontend para integrar com o django

# Criar um Docker File

$ FROM node:10.16.3 as static_builder
$ WORKDIR /app

-> Copiar os requerimentos do frontend para o docker file
-> move_build_frontend é um script criado para mover os arquivos buildados


$ COPY ./frontend/package-lock.json /app/
$ COPY ./frontend/package.json /app/
$ COPY ./move_build_frontend.sh /app/

RUN npm ci
-> Mover os arquivos buildados para dentro de um root directory

$ COPY ./frontend /app
$ ARG PUBLIC_URL=""
$ RUN npm run build
$ RUN  chmod +x /app/move_build_frontend.sh

-> Rodar os requerimentos do pipfile e copiar o que foi buildado para dentro do django

$ FROM python:3.7
$ RUN apt-get update -qq && apt-get install -y -qq libxmlsec1-dev
$ RUN pip install pipenv==2018.11.26 --no-cache-dir

$ COPY Pipfile /app/
$ COPY Pipfile.lock /app/
$ WORKDIR /app
$ RUN ["/bin/bash", "-c", "pip install --no-cache-dir -r <(pipenv lock -r)"]
$ COPY . /app
$ COPY --from=static_builder /app/build ./frontend/build

# Criar um app.json que, antes de fazer um deploy total da um collect static e migrate o banco de dados

{
  "scripts": {
    "dokku": {
      "predeploy": "python django_server/manage.py collectstatic --noinput && python django_server/manage.py migrate --noinput"
    }
  }
}

# Criando um Procfile
web: gunicorn --chdir ./django_server/ --config ./django_server/django_server/gunicorn.py django_server.wsgi


# move_build_frontend.sh
$ #!/usr/bin/env bash
$ mkdir -p build/root
$ for file in $(ls build | grep -E -v '^(index\.html|static|root)$'); do
$     mv "build/$file" build/root;
$ done