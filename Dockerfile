FROM node:10.16.3 as static_builder

WORKDIR /app

COPY ./frontend/package-lock.json /app/
COPY ./frontend/package.json /app/
RUN npm ci

COPY ./move_build_frontend.sh /app/
COPY ./frontend /app
ARG PUBLIC_URL=""
RUN npm run build
#Weird chmod, this should already be fixed
RUN  chmod +x /app/move_build_frontend.sh

FROM python:3.7

RUN apt-get update -qq && apt-get install -y -qq libxmlsec1-dev

RUN pip install pipenv==2018.11.26 --no-cache-dir

COPY Pipfile /app/
COPY Pipfile.lock /app/

WORKDIR /app
RUN ["/bin/bash", "-c", "pip install --no-cache-dir -r <(pipenv lock -r)"]

COPY . /app
COPY --from=static_builder /app/build ./frontend/build
