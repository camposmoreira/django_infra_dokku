Instalando o Dokku:

# install docker
wget -nv -O - https://get.docker.com/ | sh

# setup dokku apt repository
wget -nv -O - https://packagecloud.io/dokku/dokku/gpgkey | apt-key add -
export SOURCE="https://packagecloud.io/dokku/dokku/ubuntu/"
export OS_ID="$(lsb_release -cs 2>/dev/null || echo "trusty")"
echo "utopicvividwilyxenialyakketyzestyartfulbionic" | grep -q "$OS_ID" || OS_ID="trusty"
echo "deb $SOURCE $OS_ID main" | tee /etc/apt/sources.list.d/dokku.list
apt-get update

 # install dokku
apt-get install dokku
dokku plugin:install-dependencies --core 

# go to your server's IP and follow the web installer 
-> Dokku exibi uma pagina de installer para configurar algumas configurações padrões
(VHOST, domain e quais as chaves que estão configuradas)


