# Inicializando o Pipenv
$ pipenv --python 3.7

# Instalando bibliotecas para o projeto django
$ pipenv install django psycopg2 gunicorn python-decouple djangorestframework whitenoise

python-decouple -> Biblioteca que utilizei para me auxiliar no .env

# Iniciar Django
$ pipenv shell
$ django-admin.py startproject django_server django_server

# Mudanças no projeto django
-> Usar o postgres como database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('PSQL_DB_NAME', default=''),
        'USER': config('PSQL_DB_USER', default=''),
        'PASSWORD': config('PSQL_DB_PASS', default=''),
        'HOST': config('PSQL_DB_HOST', default=''),
        'PORT': config('PSQL_DB_PORT', default=''),
    }
}

-> Criar User costumizado
-> Adicionar accounts/djangorestframework para installed apps

# Criar o frontend react

$ npx create-react-app react_frontend
$ npm install axios --save
$ npm install urijs --save
$ npm install js-cookie --save

-> Criar uma página que faz um request para accounts e imprime o resultado

# Integrando React/Django
-> Me baseei no modelo hibrido, parecido com o performance e o powerplug
-> https://fractalideas.com/blog/making-react-and-django-play-well-together-hybrid-app-model/

- Rename BASEDIR, and include FRONTEND_DIR in settings
- Create views to catchall (Build from frontend)
- Add white noise to middleware
- add 'whitenoise.runserver_nostatic', to installed apps
- Add static files to django

STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(FRONTEND_DIR, 'build', 'static')]
STATICFILES_STORAGE = (
    'whitenoise.storage.CompressedManifestStaticFilesStorage')
STATIC_ROOT = os.path.join(BACKEND_DIR, 'static')
WHITENOISE_ROOT = os.path.join(FRONTEND_DIR, 'build', 'root')

- Adicionar Dir to template

TEMPLATES = [
    {
        'DIRS': [os.path.join(FRONTEND_DIR, 'build')],
        ...,
    },
]
