#Criar o app, 
-> Comandos executados no servidor
$ dokku apps:create djangoinfradokku

#Instalar o postgres
$ sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git

#Criar e linkar o banco de dados com o app do projeto
dokku postgres:create djangoinfradokku
dokku postgres:link djangoinfradokku djangoinfradokku

#setar algumas variaveis de .env no app
dokku config:set django_infra_dokku SECRET_KEY='Chave Secreta'

->Fazemos o collect static no app.json, desabilitamos o automatico

dokku config:set djangoinfradokku DISABLE_COLLECTSTATIC=1

# Da maquina local, podemos fazer deploy pelo git
git remote add dokku dokku@[nome_do_server]:djangoinfradokku

#Com isto, podemos dar um push da branch no servidor
git push dokku master

#No servidor, podemos rodar certor comandos, como criar um superuser
dokku run djangoinfradokku python ./django_server/manage.py createsuperuser




