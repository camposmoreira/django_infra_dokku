# django_infra_dokku

Repositorio para testes com infra, desta vez ele está aberto

# Primeiro, criar um projeto Django/React para deployar
-> Ver tutorial_projeto.md

# Segundo, criar um sudo user no servidor
-> https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart
-> Adicionar chaves para conectar com o servidor

# Terceiro, criar e configurar o dokku no servidor
-> ver tutorial_instalar_dokku

# Quarto, voltar ao projeto para configurar algumas configurações para o dokku
-> Ver tutorial_adicionar_settings

# Quinto, jogar o projeto para o dokku
-> Ver tutorial_dokku


# Construir o nginx
dokku nginx:build-config app_name


